// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCWE4B-lYC-4JIizXtmoNepZm4Xaigi_IY",
    authDomain: "oshop-1976.firebaseapp.com",
    databaseURL: "https://oshop-1976.firebaseio.com",
    projectId: "oshop-1976",
    storageBucket: "oshop-1976.appspot.com",
    messagingSenderId: "738383076575"
  }
};
