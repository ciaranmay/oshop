import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
courses=[
  {
  title: 'title',
  rating: 1.2345,
  students: 31245,
  price: 190.33,
  date: new Date(2017, 3, 1)
  
},  {
  title: 'title2',
  rating: 1.2345,
  students: 31245,
  price: 190.334444,
  date: new Date(2017, 3, 1)
  
}
]
constructor() { }


  ngOnInit() {
  }

}
