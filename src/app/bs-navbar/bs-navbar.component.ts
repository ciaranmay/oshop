import { AppUser } from './../models/app.user';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({ selector: 'bs-navbar', templateUrl: './bs-navbar.component.html', styleUrls: ['./bs-navbar.component.css'] })
export class BsNavbarComponent {
  appUser: AppUser;
  public isCollapsed = true;

  constructor(private auth: AuthService) {
    auth.appUser$.subscribe(appUser => this.appUser = appUser)
  }

  logOut() {
    this.auth.logOut();
  }
}
